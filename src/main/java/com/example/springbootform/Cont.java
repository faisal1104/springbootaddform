package com.example.springbootform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class Cont {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/")
        public String s(Model m)
    {
        List<User> userL = userRepository.findAll();
        m.addAttribute("allUser", userL);
        return "view";
        }

        @GetMapping("/add")
        public String add1(Model m){

        m.addAttribute("em", new User());

        return "add";
        }

        @PostMapping("/save")
      public String add(@ModelAttribute("em") User user){

        userRepository.save(user);

        return "view";
        }

}
