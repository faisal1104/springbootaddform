package com.example.springbootform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class SpringbooformApplication {

    public static void main(String[] args) {


        SpringApplication.run(new Class[]{SpringbooformApplication.class,Initial.class}, args);
    }

}
