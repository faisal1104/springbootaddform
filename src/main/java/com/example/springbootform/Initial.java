package com.example.springbootform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

public class Initial implements CommandLineRunner {

    @Autowired
    UserRepository userRepository;
    @Override
    public void run(String... args) throws Exception {
        userRepository.deleteAll();

        User user1 = new User("Faisal", "Employee");
        User user2 = new User("Fahim", "Manager");
        User user3 = new User("Jamal", "Employee");

        userRepository.save(user1);

        userRepository.save(user2);

        userRepository.save(user3);



    }
}
